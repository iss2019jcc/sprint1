/*
 * generated by Xtext 2.17.0
 */
package it.unibo.jcc.xtext.scoping


/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class SimpleAgentScopeProvider extends AbstractSimpleAgentScopeProvider {

}
