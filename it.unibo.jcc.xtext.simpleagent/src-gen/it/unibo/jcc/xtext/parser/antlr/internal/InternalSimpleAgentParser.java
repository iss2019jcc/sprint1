package it.unibo.jcc.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import it.unibo.jcc.xtext.services.SimpleAgentGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSimpleAgentParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'SimpleAgent'", "'Effect'", "'Behavior'", "'{'", "'}'", "'Action'", "','", "'w'", "'a'", "'d'", "'onEffect'", "'('", "')'", "'['", "']'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalSimpleAgentParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSimpleAgentParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSimpleAgentParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSimpleAgent.g"; }



     	private SimpleAgentGrammarAccess grammarAccess;

        public InternalSimpleAgentParser(TokenStream input, SimpleAgentGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "SimpleAgent";
       	}

       	@Override
       	protected SimpleAgentGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleSimpleAgent"
    // InternalSimpleAgent.g:64:1: entryRuleSimpleAgent returns [EObject current=null] : iv_ruleSimpleAgent= ruleSimpleAgent EOF ;
    public final EObject entryRuleSimpleAgent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleAgent = null;


        try {
            // InternalSimpleAgent.g:64:52: (iv_ruleSimpleAgent= ruleSimpleAgent EOF )
            // InternalSimpleAgent.g:65:2: iv_ruleSimpleAgent= ruleSimpleAgent EOF
            {
             newCompositeNode(grammarAccess.getSimpleAgentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimpleAgent=ruleSimpleAgent();

            state._fsp--;

             current =iv_ruleSimpleAgent; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleAgent"


    // $ANTLR start "ruleSimpleAgent"
    // InternalSimpleAgent.g:71:1: ruleSimpleAgent returns [EObject current=null] : (otherlv_0= 'SimpleAgent' ( (lv_name_1_0= RULE_ID ) ) ( (lv_start_2_0= ruleAnyAction ) )? ( (lv_effects_3_0= ruleEffect ) )* ( (lv_actionsSection_4_0= ruleActions ) ) ) ;
    public final EObject ruleSimpleAgent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        EObject lv_start_2_0 = null;

        EObject lv_effects_3_0 = null;

        EObject lv_actionsSection_4_0 = null;



        	enterRule();

        try {
            // InternalSimpleAgent.g:77:2: ( (otherlv_0= 'SimpleAgent' ( (lv_name_1_0= RULE_ID ) ) ( (lv_start_2_0= ruleAnyAction ) )? ( (lv_effects_3_0= ruleEffect ) )* ( (lv_actionsSection_4_0= ruleActions ) ) ) )
            // InternalSimpleAgent.g:78:2: (otherlv_0= 'SimpleAgent' ( (lv_name_1_0= RULE_ID ) ) ( (lv_start_2_0= ruleAnyAction ) )? ( (lv_effects_3_0= ruleEffect ) )* ( (lv_actionsSection_4_0= ruleActions ) ) )
            {
            // InternalSimpleAgent.g:78:2: (otherlv_0= 'SimpleAgent' ( (lv_name_1_0= RULE_ID ) ) ( (lv_start_2_0= ruleAnyAction ) )? ( (lv_effects_3_0= ruleEffect ) )* ( (lv_actionsSection_4_0= ruleActions ) ) )
            // InternalSimpleAgent.g:79:3: otherlv_0= 'SimpleAgent' ( (lv_name_1_0= RULE_ID ) ) ( (lv_start_2_0= ruleAnyAction ) )? ( (lv_effects_3_0= ruleEffect ) )* ( (lv_actionsSection_4_0= ruleActions ) )
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getSimpleAgentAccess().getSimpleAgentKeyword_0());
            		
            // InternalSimpleAgent.g:83:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalSimpleAgent.g:84:4: (lv_name_1_0= RULE_ID )
            {
            // InternalSimpleAgent.g:84:4: (lv_name_1_0= RULE_ID )
            // InternalSimpleAgent.g:85:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getSimpleAgentAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSimpleAgentRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalSimpleAgent.g:101:3: ( (lv_start_2_0= ruleAnyAction ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==24) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalSimpleAgent.g:102:4: (lv_start_2_0= ruleAnyAction )
                    {
                    // InternalSimpleAgent.g:102:4: (lv_start_2_0= ruleAnyAction )
                    // InternalSimpleAgent.g:103:5: lv_start_2_0= ruleAnyAction
                    {

                    					newCompositeNode(grammarAccess.getSimpleAgentAccess().getStartAnyActionParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_4);
                    lv_start_2_0=ruleAnyAction();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getSimpleAgentRule());
                    					}
                    					set(
                    						current,
                    						"start",
                    						lv_start_2_0,
                    						"it.unibo.jcc.xtext.SimpleAgent.AnyAction");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalSimpleAgent.g:120:3: ( (lv_effects_3_0= ruleEffect ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==12) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalSimpleAgent.g:121:4: (lv_effects_3_0= ruleEffect )
            	    {
            	    // InternalSimpleAgent.g:121:4: (lv_effects_3_0= ruleEffect )
            	    // InternalSimpleAgent.g:122:5: lv_effects_3_0= ruleEffect
            	    {

            	    					newCompositeNode(grammarAccess.getSimpleAgentAccess().getEffectsEffectParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_effects_3_0=ruleEffect();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getSimpleAgentRule());
            	    					}
            	    					add(
            	    						current,
            	    						"effects",
            	    						lv_effects_3_0,
            	    						"it.unibo.jcc.xtext.SimpleAgent.Effect");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalSimpleAgent.g:139:3: ( (lv_actionsSection_4_0= ruleActions ) )
            // InternalSimpleAgent.g:140:4: (lv_actionsSection_4_0= ruleActions )
            {
            // InternalSimpleAgent.g:140:4: (lv_actionsSection_4_0= ruleActions )
            // InternalSimpleAgent.g:141:5: lv_actionsSection_4_0= ruleActions
            {

            					newCompositeNode(grammarAccess.getSimpleAgentAccess().getActionsSectionActionsParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_2);
            lv_actionsSection_4_0=ruleActions();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSimpleAgentRule());
            					}
            					set(
            						current,
            						"actionsSection",
            						lv_actionsSection_4_0,
            						"it.unibo.jcc.xtext.SimpleAgent.Actions");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleAgent"


    // $ANTLR start "entryRuleEffect"
    // InternalSimpleAgent.g:162:1: entryRuleEffect returns [EObject current=null] : iv_ruleEffect= ruleEffect EOF ;
    public final EObject entryRuleEffect() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEffect = null;


        try {
            // InternalSimpleAgent.g:162:47: (iv_ruleEffect= ruleEffect EOF )
            // InternalSimpleAgent.g:163:2: iv_ruleEffect= ruleEffect EOF
            {
             newCompositeNode(grammarAccess.getEffectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEffect=ruleEffect();

            state._fsp--;

             current =iv_ruleEffect; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEffect"


    // $ANTLR start "ruleEffect"
    // InternalSimpleAgent.g:169:1: ruleEffect returns [EObject current=null] : (otherlv_0= 'Effect' ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleEffect() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalSimpleAgent.g:175:2: ( (otherlv_0= 'Effect' ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalSimpleAgent.g:176:2: (otherlv_0= 'Effect' ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalSimpleAgent.g:176:2: (otherlv_0= 'Effect' ( (lv_name_1_0= RULE_ID ) ) )
            // InternalSimpleAgent.g:177:3: otherlv_0= 'Effect' ( (lv_name_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,12,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getEffectAccess().getEffectKeyword_0());
            		
            // InternalSimpleAgent.g:181:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalSimpleAgent.g:182:4: (lv_name_1_0= RULE_ID )
            {
            // InternalSimpleAgent.g:182:4: (lv_name_1_0= RULE_ID )
            // InternalSimpleAgent.g:183:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getEffectAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEffectRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEffect"


    // $ANTLR start "entryRuleActions"
    // InternalSimpleAgent.g:203:1: entryRuleActions returns [EObject current=null] : iv_ruleActions= ruleActions EOF ;
    public final EObject entryRuleActions() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActions = null;


        try {
            // InternalSimpleAgent.g:203:48: (iv_ruleActions= ruleActions EOF )
            // InternalSimpleAgent.g:204:2: iv_ruleActions= ruleActions EOF
            {
             newCompositeNode(grammarAccess.getActionsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleActions=ruleActions();

            state._fsp--;

             current =iv_ruleActions; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActions"


    // $ANTLR start "ruleActions"
    // InternalSimpleAgent.g:210:1: ruleActions returns [EObject current=null] : ( () otherlv_1= 'Behavior' otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) )* otherlv_4= '}' ) ;
    public final EObject ruleActions() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_actions_3_0 = null;



        	enterRule();

        try {
            // InternalSimpleAgent.g:216:2: ( ( () otherlv_1= 'Behavior' otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) )* otherlv_4= '}' ) )
            // InternalSimpleAgent.g:217:2: ( () otherlv_1= 'Behavior' otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) )* otherlv_4= '}' )
            {
            // InternalSimpleAgent.g:217:2: ( () otherlv_1= 'Behavior' otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) )* otherlv_4= '}' )
            // InternalSimpleAgent.g:218:3: () otherlv_1= 'Behavior' otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) )* otherlv_4= '}'
            {
            // InternalSimpleAgent.g:218:3: ()
            // InternalSimpleAgent.g:219:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getActionsAccess().getActionsAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,13,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getActionsAccess().getBehaviorKeyword_1());
            		
            otherlv_2=(Token)match(input,14,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getActionsAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalSimpleAgent.g:233:3: ( (lv_actions_3_0= ruleAction ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==16) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalSimpleAgent.g:234:4: (lv_actions_3_0= ruleAction )
            	    {
            	    // InternalSimpleAgent.g:234:4: (lv_actions_3_0= ruleAction )
            	    // InternalSimpleAgent.g:235:5: lv_actions_3_0= ruleAction
            	    {

            	    					newCompositeNode(grammarAccess.getActionsAccess().getActionsActionParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_actions_3_0=ruleAction();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getActionsRule());
            	    					}
            	    					add(
            	    						current,
            	    						"actions",
            	    						lv_actions_3_0,
            	    						"it.unibo.jcc.xtext.SimpleAgent.Action");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_4=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getActionsAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActions"


    // $ANTLR start "entryRuleAction"
    // InternalSimpleAgent.g:260:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalSimpleAgent.g:260:47: (iv_ruleAction= ruleAction EOF )
            // InternalSimpleAgent.g:261:2: iv_ruleAction= ruleAction EOF
            {
             newCompositeNode(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;

             current =iv_ruleAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalSimpleAgent.g:267:1: ruleAction returns [EObject current=null] : (otherlv_0= 'Action' ( (lv_actions_1_0= ruleActionName ) ) (otherlv_2= ',' ( (lv_actions_3_0= ruleActionName ) ) )* ( (lv_body_4_0= ruleAnyAction ) ) ( (lv_on_5_0= ruleOnEffect ) )* ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_actions_1_0 = null;

        EObject lv_actions_3_0 = null;

        EObject lv_body_4_0 = null;

        EObject lv_on_5_0 = null;



        	enterRule();

        try {
            // InternalSimpleAgent.g:273:2: ( (otherlv_0= 'Action' ( (lv_actions_1_0= ruleActionName ) ) (otherlv_2= ',' ( (lv_actions_3_0= ruleActionName ) ) )* ( (lv_body_4_0= ruleAnyAction ) ) ( (lv_on_5_0= ruleOnEffect ) )* ) )
            // InternalSimpleAgent.g:274:2: (otherlv_0= 'Action' ( (lv_actions_1_0= ruleActionName ) ) (otherlv_2= ',' ( (lv_actions_3_0= ruleActionName ) ) )* ( (lv_body_4_0= ruleAnyAction ) ) ( (lv_on_5_0= ruleOnEffect ) )* )
            {
            // InternalSimpleAgent.g:274:2: (otherlv_0= 'Action' ( (lv_actions_1_0= ruleActionName ) ) (otherlv_2= ',' ( (lv_actions_3_0= ruleActionName ) ) )* ( (lv_body_4_0= ruleAnyAction ) ) ( (lv_on_5_0= ruleOnEffect ) )* )
            // InternalSimpleAgent.g:275:3: otherlv_0= 'Action' ( (lv_actions_1_0= ruleActionName ) ) (otherlv_2= ',' ( (lv_actions_3_0= ruleActionName ) ) )* ( (lv_body_4_0= ruleAnyAction ) ) ( (lv_on_5_0= ruleOnEffect ) )*
            {
            otherlv_0=(Token)match(input,16,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getActionAccess().getActionKeyword_0());
            		
            // InternalSimpleAgent.g:279:3: ( (lv_actions_1_0= ruleActionName ) )
            // InternalSimpleAgent.g:280:4: (lv_actions_1_0= ruleActionName )
            {
            // InternalSimpleAgent.g:280:4: (lv_actions_1_0= ruleActionName )
            // InternalSimpleAgent.g:281:5: lv_actions_1_0= ruleActionName
            {

            					newCompositeNode(grammarAccess.getActionAccess().getActionsActionNameParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_8);
            lv_actions_1_0=ruleActionName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getActionRule());
            					}
            					add(
            						current,
            						"actions",
            						lv_actions_1_0,
            						"it.unibo.jcc.xtext.SimpleAgent.ActionName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSimpleAgent.g:298:3: (otherlv_2= ',' ( (lv_actions_3_0= ruleActionName ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==17) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalSimpleAgent.g:299:4: otherlv_2= ',' ( (lv_actions_3_0= ruleActionName ) )
            	    {
            	    otherlv_2=(Token)match(input,17,FOLLOW_7); 

            	    				newLeafNode(otherlv_2, grammarAccess.getActionAccess().getCommaKeyword_2_0());
            	    			
            	    // InternalSimpleAgent.g:303:4: ( (lv_actions_3_0= ruleActionName ) )
            	    // InternalSimpleAgent.g:304:5: (lv_actions_3_0= ruleActionName )
            	    {
            	    // InternalSimpleAgent.g:304:5: (lv_actions_3_0= ruleActionName )
            	    // InternalSimpleAgent.g:305:6: lv_actions_3_0= ruleActionName
            	    {

            	    						newCompositeNode(grammarAccess.getActionAccess().getActionsActionNameParserRuleCall_2_1_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_actions_3_0=ruleActionName();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getActionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"actions",
            	    							lv_actions_3_0,
            	    							"it.unibo.jcc.xtext.SimpleAgent.ActionName");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            // InternalSimpleAgent.g:323:3: ( (lv_body_4_0= ruleAnyAction ) )
            // InternalSimpleAgent.g:324:4: (lv_body_4_0= ruleAnyAction )
            {
            // InternalSimpleAgent.g:324:4: (lv_body_4_0= ruleAnyAction )
            // InternalSimpleAgent.g:325:5: lv_body_4_0= ruleAnyAction
            {

            					newCompositeNode(grammarAccess.getActionAccess().getBodyAnyActionParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_9);
            lv_body_4_0=ruleAnyAction();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getActionRule());
            					}
            					set(
            						current,
            						"body",
            						lv_body_4_0,
            						"it.unibo.jcc.xtext.SimpleAgent.AnyAction");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalSimpleAgent.g:342:3: ( (lv_on_5_0= ruleOnEffect ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==21) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalSimpleAgent.g:343:4: (lv_on_5_0= ruleOnEffect )
            	    {
            	    // InternalSimpleAgent.g:343:4: (lv_on_5_0= ruleOnEffect )
            	    // InternalSimpleAgent.g:344:5: lv_on_5_0= ruleOnEffect
            	    {

            	    					newCompositeNode(grammarAccess.getActionAccess().getOnOnEffectParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_9);
            	    lv_on_5_0=ruleOnEffect();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getActionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"on",
            	    						lv_on_5_0,
            	    						"it.unibo.jcc.xtext.SimpleAgent.OnEffect");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleActionName"
    // InternalSimpleAgent.g:365:1: entryRuleActionName returns [EObject current=null] : iv_ruleActionName= ruleActionName EOF ;
    public final EObject entryRuleActionName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActionName = null;


        try {
            // InternalSimpleAgent.g:365:51: (iv_ruleActionName= ruleActionName EOF )
            // InternalSimpleAgent.g:366:2: iv_ruleActionName= ruleActionName EOF
            {
             newCompositeNode(grammarAccess.getActionNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleActionName=ruleActionName();

            state._fsp--;

             current =iv_ruleActionName; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActionName"


    // $ANTLR start "ruleActionName"
    // InternalSimpleAgent.g:372:1: ruleActionName returns [EObject current=null] : ( ( (lv_value_0_1= 'w' | lv_value_0_2= 'a' | lv_value_0_3= 'd' ) ) ) ;
    public final EObject ruleActionName() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_1=null;
        Token lv_value_0_2=null;
        Token lv_value_0_3=null;


        	enterRule();

        try {
            // InternalSimpleAgent.g:378:2: ( ( ( (lv_value_0_1= 'w' | lv_value_0_2= 'a' | lv_value_0_3= 'd' ) ) ) )
            // InternalSimpleAgent.g:379:2: ( ( (lv_value_0_1= 'w' | lv_value_0_2= 'a' | lv_value_0_3= 'd' ) ) )
            {
            // InternalSimpleAgent.g:379:2: ( ( (lv_value_0_1= 'w' | lv_value_0_2= 'a' | lv_value_0_3= 'd' ) ) )
            // InternalSimpleAgent.g:380:3: ( (lv_value_0_1= 'w' | lv_value_0_2= 'a' | lv_value_0_3= 'd' ) )
            {
            // InternalSimpleAgent.g:380:3: ( (lv_value_0_1= 'w' | lv_value_0_2= 'a' | lv_value_0_3= 'd' ) )
            // InternalSimpleAgent.g:381:4: (lv_value_0_1= 'w' | lv_value_0_2= 'a' | lv_value_0_3= 'd' )
            {
            // InternalSimpleAgent.g:381:4: (lv_value_0_1= 'w' | lv_value_0_2= 'a' | lv_value_0_3= 'd' )
            int alt6=3;
            switch ( input.LA(1) ) {
            case 18:
                {
                alt6=1;
                }
                break;
            case 19:
                {
                alt6=2;
                }
                break;
            case 20:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalSimpleAgent.g:382:5: lv_value_0_1= 'w'
                    {
                    lv_value_0_1=(Token)match(input,18,FOLLOW_2); 

                    					newLeafNode(lv_value_0_1, grammarAccess.getActionNameAccess().getValueWKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getActionNameRule());
                    					}
                    					setWithLastConsumed(current, "value", lv_value_0_1, null);
                    				

                    }
                    break;
                case 2 :
                    // InternalSimpleAgent.g:393:5: lv_value_0_2= 'a'
                    {
                    lv_value_0_2=(Token)match(input,19,FOLLOW_2); 

                    					newLeafNode(lv_value_0_2, grammarAccess.getActionNameAccess().getValueAKeyword_0_1());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getActionNameRule());
                    					}
                    					setWithLastConsumed(current, "value", lv_value_0_2, null);
                    				

                    }
                    break;
                case 3 :
                    // InternalSimpleAgent.g:404:5: lv_value_0_3= 'd'
                    {
                    lv_value_0_3=(Token)match(input,20,FOLLOW_2); 

                    					newLeafNode(lv_value_0_3, grammarAccess.getActionNameAccess().getValueDKeyword_0_2());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getActionNameRule());
                    					}
                    					setWithLastConsumed(current, "value", lv_value_0_3, null);
                    				

                    }
                    break;

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActionName"


    // $ANTLR start "entryRuleOnEffect"
    // InternalSimpleAgent.g:420:1: entryRuleOnEffect returns [EObject current=null] : iv_ruleOnEffect= ruleOnEffect EOF ;
    public final EObject entryRuleOnEffect() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOnEffect = null;


        try {
            // InternalSimpleAgent.g:420:49: (iv_ruleOnEffect= ruleOnEffect EOF )
            // InternalSimpleAgent.g:421:2: iv_ruleOnEffect= ruleOnEffect EOF
            {
             newCompositeNode(grammarAccess.getOnEffectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOnEffect=ruleOnEffect();

            state._fsp--;

             current =iv_ruleOnEffect; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOnEffect"


    // $ANTLR start "ruleOnEffect"
    // InternalSimpleAgent.g:427:1: ruleOnEffect returns [EObject current=null] : (otherlv_0= 'onEffect' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' ( (lv_body_4_0= ruleAnyAction ) ) ) ;
    public final EObject ruleOnEffect() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        EObject lv_body_4_0 = null;



        	enterRule();

        try {
            // InternalSimpleAgent.g:433:2: ( (otherlv_0= 'onEffect' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' ( (lv_body_4_0= ruleAnyAction ) ) ) )
            // InternalSimpleAgent.g:434:2: (otherlv_0= 'onEffect' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' ( (lv_body_4_0= ruleAnyAction ) ) )
            {
            // InternalSimpleAgent.g:434:2: (otherlv_0= 'onEffect' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' ( (lv_body_4_0= ruleAnyAction ) ) )
            // InternalSimpleAgent.g:435:3: otherlv_0= 'onEffect' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' ( (lv_body_4_0= ruleAnyAction ) )
            {
            otherlv_0=(Token)match(input,21,FOLLOW_10); 

            			newLeafNode(otherlv_0, grammarAccess.getOnEffectAccess().getOnEffectKeyword_0());
            		
            otherlv_1=(Token)match(input,22,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getOnEffectAccess().getLeftParenthesisKeyword_1());
            		
            // InternalSimpleAgent.g:443:3: ( (otherlv_2= RULE_ID ) )
            // InternalSimpleAgent.g:444:4: (otherlv_2= RULE_ID )
            {
            // InternalSimpleAgent.g:444:4: (otherlv_2= RULE_ID )
            // InternalSimpleAgent.g:445:5: otherlv_2= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOnEffectRule());
            					}
            				
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_11); 

            					newLeafNode(otherlv_2, grammarAccess.getOnEffectAccess().getEffectEffectCrossReference_2_0());
            				

            }


            }

            otherlv_3=(Token)match(input,23,FOLLOW_12); 

            			newLeafNode(otherlv_3, grammarAccess.getOnEffectAccess().getRightParenthesisKeyword_3());
            		
            // InternalSimpleAgent.g:460:3: ( (lv_body_4_0= ruleAnyAction ) )
            // InternalSimpleAgent.g:461:4: (lv_body_4_0= ruleAnyAction )
            {
            // InternalSimpleAgent.g:461:4: (lv_body_4_0= ruleAnyAction )
            // InternalSimpleAgent.g:462:5: lv_body_4_0= ruleAnyAction
            {

            					newCompositeNode(grammarAccess.getOnEffectAccess().getBodyAnyActionParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_2);
            lv_body_4_0=ruleAnyAction();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getOnEffectRule());
            					}
            					set(
            						current,
            						"body",
            						lv_body_4_0,
            						"it.unibo.jcc.xtext.SimpleAgent.AnyAction");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOnEffect"


    // $ANTLR start "entryRuleAnyAction"
    // InternalSimpleAgent.g:483:1: entryRuleAnyAction returns [EObject current=null] : iv_ruleAnyAction= ruleAnyAction EOF ;
    public final EObject entryRuleAnyAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnyAction = null;


        try {
            // InternalSimpleAgent.g:483:50: (iv_ruleAnyAction= ruleAnyAction EOF )
            // InternalSimpleAgent.g:484:2: iv_ruleAnyAction= ruleAnyAction EOF
            {
             newCompositeNode(grammarAccess.getAnyActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnyAction=ruleAnyAction();

            state._fsp--;

             current =iv_ruleAnyAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnyAction"


    // $ANTLR start "ruleAnyAction"
    // InternalSimpleAgent.g:490:1: ruleAnyAction returns [EObject current=null] : (otherlv_0= '[' ( (lv_body_1_0= RULE_STRING ) ) otherlv_2= ']' ) ;
    public final EObject ruleAnyAction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_body_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalSimpleAgent.g:496:2: ( (otherlv_0= '[' ( (lv_body_1_0= RULE_STRING ) ) otherlv_2= ']' ) )
            // InternalSimpleAgent.g:497:2: (otherlv_0= '[' ( (lv_body_1_0= RULE_STRING ) ) otherlv_2= ']' )
            {
            // InternalSimpleAgent.g:497:2: (otherlv_0= '[' ( (lv_body_1_0= RULE_STRING ) ) otherlv_2= ']' )
            // InternalSimpleAgent.g:498:3: otherlv_0= '[' ( (lv_body_1_0= RULE_STRING ) ) otherlv_2= ']'
            {
            otherlv_0=(Token)match(input,24,FOLLOW_13); 

            			newLeafNode(otherlv_0, grammarAccess.getAnyActionAccess().getLeftSquareBracketKeyword_0());
            		
            // InternalSimpleAgent.g:502:3: ( (lv_body_1_0= RULE_STRING ) )
            // InternalSimpleAgent.g:503:4: (lv_body_1_0= RULE_STRING )
            {
            // InternalSimpleAgent.g:503:4: (lv_body_1_0= RULE_STRING )
            // InternalSimpleAgent.g:504:5: lv_body_1_0= RULE_STRING
            {
            lv_body_1_0=(Token)match(input,RULE_STRING,FOLLOW_14); 

            					newLeafNode(lv_body_1_0, grammarAccess.getAnyActionAccess().getBodySTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAnyActionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"body",
            						lv_body_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_2=(Token)match(input,25,FOLLOW_2); 

            			newLeafNode(otherlv_2, grammarAccess.getAnyActionAccess().getRightSquareBracketKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnyAction"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000001003000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x00000000001C0000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000001020000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000002000000L});

}