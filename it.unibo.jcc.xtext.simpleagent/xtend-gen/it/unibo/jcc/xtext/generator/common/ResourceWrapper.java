package it.unibo.jcc.xtext.generator.common;

import com.google.common.collect.Iterators;
import it.unibo.jcc.xtext.simpleAgent.Action;
import it.unibo.jcc.xtext.simpleAgent.Actions;
import it.unibo.jcc.xtext.simpleAgent.SimpleAgent;
import java.util.Iterator;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

@SuppressWarnings("all")
public class ResourceWrapper {
  private Resource resource;
  
  public ResourceWrapper(final Resource resource) {
    this.resource = resource;
  }
  
  public String getAgentName() {
    final Function1<SimpleAgent, String> _function = (SimpleAgent it) -> {
      return it.getName();
    };
    return IteratorExtensions.<String>last(IteratorExtensions.<SimpleAgent, String>map(Iterators.<SimpleAgent>filter(this.resource.getAllContents(), SimpleAgent.class), _function)).toLowerCase();
  }
  
  public Actions getActionsClause() {
    return IteratorExtensions.<Actions>last(Iterators.<Actions>filter(this.resource.getAllContents(), Actions.class));
  }
  
  public Iterator<Action> getActions() {
    return Iterators.<Action>filter(this.resource.getAllContents(), Action.class);
  }
}
