/**
 * generated by Xtext 2.17.0
 */
package it.unibo.jcc.xtext.simpleAgent.impl;

import it.unibo.jcc.xtext.simpleAgent.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SimpleAgentFactoryImpl extends EFactoryImpl implements SimpleAgentFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static SimpleAgentFactory init()
  {
    try
    {
      SimpleAgentFactory theSimpleAgentFactory = (SimpleAgentFactory)EPackage.Registry.INSTANCE.getEFactory(SimpleAgentPackage.eNS_URI);
      if (theSimpleAgentFactory != null)
      {
        return theSimpleAgentFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new SimpleAgentFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimpleAgentFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case SimpleAgentPackage.SIMPLE_AGENT: return createSimpleAgent();
      case SimpleAgentPackage.EFFECT: return createEffect();
      case SimpleAgentPackage.ACTIONS: return createActions();
      case SimpleAgentPackage.ACTION: return createAction();
      case SimpleAgentPackage.ACTION_NAME: return createActionName();
      case SimpleAgentPackage.ON_EFFECT: return createOnEffect();
      case SimpleAgentPackage.ANY_ACTION: return createAnyAction();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SimpleAgent createSimpleAgent()
  {
    SimpleAgentImpl simpleAgent = new SimpleAgentImpl();
    return simpleAgent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Effect createEffect()
  {
    EffectImpl effect = new EffectImpl();
    return effect;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Actions createActions()
  {
    ActionsImpl actions = new ActionsImpl();
    return actions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Action createAction()
  {
    ActionImpl action = new ActionImpl();
    return action;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ActionName createActionName()
  {
    ActionNameImpl actionName = new ActionNameImpl();
    return actionName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OnEffect createOnEffect()
  {
    OnEffectImpl onEffect = new OnEffectImpl();
    return onEffect;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AnyAction createAnyAction()
  {
    AnyActionImpl anyAction = new AnyActionImpl();
    return anyAction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SimpleAgentPackage getSimpleAgentPackage()
  {
    return (SimpleAgentPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static SimpleAgentPackage getPackage()
  {
    return SimpleAgentPackage.eINSTANCE;
  }

} //SimpleAgentFactoryImpl
