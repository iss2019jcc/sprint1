package it.unibo.jcc.xtext.generator.common

import org.eclipse.emf.ecore.resource.Resource
import it.unibo.jcc.xtext.simpleAgent.SimpleAgent
import it.unibo.jcc.xtext.simpleAgent.Action
import java.util.Iterator 
import it.unibo.jcc.xtext.simpleAgent.Actions

class ResourceWrapper {
	
	Resource resource
	
	new(Resource resource) {
		this.resource = resource
	}
	
	def String getAgentName() {
		return resource.allContents.filter(SimpleAgent).map[name].last.toLowerCase
	}
	
	def Actions getActionsClause() {
		return resource.allContents.filter(Actions).last
	}
	
	def Iterator<Action> getActions() {
		return resource.allContents.filter(Action)
	}
}