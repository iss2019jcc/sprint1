/**
 * generated by Xtext 2.17.0
 */
package it.unibo.jcc.xtext.simpleAgent;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actions</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link it.unibo.jcc.xtext.simpleAgent.Actions#getActions <em>Actions</em>}</li>
 * </ul>
 *
 * @see it.unibo.jcc.xtext.simpleAgent.SimpleAgentPackage#getActions()
 * @model
 * @generated
 */
public interface Actions extends EObject
{
  /**
   * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
   * The list contents are of type {@link it.unibo.jcc.xtext.simpleAgent.Action}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Actions</em>' containment reference list.
   * @see it.unibo.jcc.xtext.simpleAgent.SimpleAgentPackage#getActions_Actions()
   * @model containment="true"
   * @generated
   */
  EList<Action> getActions();

} // Actions
