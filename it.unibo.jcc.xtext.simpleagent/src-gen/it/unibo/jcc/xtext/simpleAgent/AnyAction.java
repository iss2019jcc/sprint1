/**
 * generated by Xtext 2.17.0
 */
package it.unibo.jcc.xtext.simpleAgent;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Any Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link it.unibo.jcc.xtext.simpleAgent.AnyAction#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see it.unibo.jcc.xtext.simpleAgent.SimpleAgentPackage#getAnyAction()
 * @model
 * @generated
 */
public interface AnyAction extends EObject
{
  /**
   * Returns the value of the '<em><b>Body</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Body</em>' attribute.
   * @see #setBody(String)
   * @see it.unibo.jcc.xtext.simpleAgent.SimpleAgentPackage#getAnyAction_Body()
   * @model
   * @generated
   */
  String getBody();

  /**
   * Sets the value of the '{@link it.unibo.jcc.xtext.simpleAgent.AnyAction#getBody <em>Body</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Body</em>' attribute.
   * @see #getBody()
   * @generated
   */
  void setBody(String value);

} // AnyAction
